<?php
/**
 * Curse Inc.
 * User Engagement
 * User Engagement Mediawiki Settings
 *
 * @author		Alex Smith
 * @copyright	(c) 2013 Curse Inc.
 * @license		GNU General Public License v2.0 or later
 * @package		User Engagement
 * @link		https://gitlab.com/hydrawiki
 *
 **/

if ( function_exists( 'wfLoadExtension' ) ) {
	wfLoadExtension( 'UserEngagement' );
	// Keep i18n globals so mergeMessageFileList.php doesn't break
	$wgMessagesDirs['UserEngagement'] = __DIR__ . '/i18n';
	wfWarn(
		'Deprecated PHP entry point used for UserEngagement extension. Please use wfLoadExtension instead, ' .
		'see https://www.mediawiki.org/wiki/Extension_registration for more details.'
	);
	return;
 } else {
	die( 'This version of the UserEngagement extension requires MediaWiki 1.25+' );
}
