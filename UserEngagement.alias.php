<?php
/**
 * Curse Inc.
 * User Engagement
 * User Engagement Aliases
 *
 * @author		Alex Smith
 * @copyright	(c) 2013 Curse Inc.
 * @license		GNU General Public License v2.0 or later
 * @package		User Engagement
 * @link		https://gitlab.com/hydrawiki
 *
 **/

$specialPageAliases = [];

/** English (English) */
$specialPageAliases['en'] = [
	'UserEngagement' => ['UserEngagement'],
	'UserEngagementLog' => ['UserEngagementLog']
];
?>