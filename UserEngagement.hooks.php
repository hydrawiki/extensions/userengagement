<?php
/**
 * Curse Inc.
 * User Engagement
 * User Engagement Language Strings
 *
 * @author		Alex Smith
 * @copyright	(c) 2013 Curse Inc.
 * @license		GNU General Public License v2.0 or later
 * @package		User Engagement
 * @link		https://gitlab.com/hydrawiki
 *
 **/

class UserEngagementHooks {
	/**
	 * Setups and Modifies Database Information
	 *
	 * @access	public
	 * @param	object	[Optional] DatabaseUpdater Object
	 * @return	boolean	true
	 */
	static public function onLoadExtensionSchemaUpdates($updater = null) {
		$extDir = __DIR__;

		//Tables
		$updater->addExtensionUpdate(['addTable', 'user_engagements', "{$extDir}/install/sql/add_table_user_engagements.sql", true]);

		//2016-07-07
		$updater->addExtensionUpdate(['dropTable', 'user_engagement_logs', "{$extDir}/upgrade/sql/drop_table_user_engagement_logs.sql", true]);
		$updater->addExtensionUpdate(['modifyField', 'user_engagements', 'x_edits_sent', "{$extDir}/upgrade/sql/drop_table_user_engagements.sql", true]);

		return true;
	}
}
?>