function UserEngagement($) {
	'use strict';

	var self = this;

	this.init = function() {
		self.initEngaged();
	};

	this.initEngaged = function() {
		$('button.engaged').on('click', function(e) {
			var form = $(this).parents('form');

			$('.engagement_notes', form).show();
		});

		$('button.cancel_engagement').on('click', function(e) {
			var form = $(this).parents('form');

			$('.engagement_notes', form).hide();
		});

		$('button.save_engagement').on('click', function(e) {
			var form = $(this).parents('form');

			var params = {
				globalId: $('input[name="global_id"]', form).val(),
				siteKey:  $('input[name="site_key"]', form).val(),
				message: $('textarea[name="message"]', form).val(),
				points: $('input[name="historical_points"]', form).val(),
				token: mw.user.tokens.get('editToken')
			};
			self.ajax('engaged', params).done(
				function(json, resp) {
					if (!json.hasOwnProperty('success') || json.success == false) {
						var errorString = "";
						for (var error in json.errors) {
							if (json.errors.hasOwnProperty(error)) {
								errorString = errorString + json.errors[error];
							}
						}
						alert(errorString);
					} else {
						$('.engagement_notes', form).hide();
					}
				}
			);
		});
	};

	this.ajax = function(method, params) {
		params.action = 'userengagement';
		params.do = method;
		params.formatversion = 2;
		return (new mw.Api()).post(params);
	};
}

var UE = new UserEngagement(jQuery);
jQuery(document).ready(UE.init);