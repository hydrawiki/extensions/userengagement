CREATE TABLE IF NOT EXISTS `user_engagements` (
  `ueid` int(14) NOT NULL,
  `global_id` int(14) DEFAULT NULL,
  `site_key` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `historical_points` int(14) NOT NULL DEFAULT '0',
  `sent_timestamp` int(14) DEFAULT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `actor_id` int(10) NOT NULL
) /*$wgDBTableOptions*/;

ALTER TABLE `user_engagements` ADD PRIMARY KEY (`ueid`), ADD KEY `global_id_site_key` (`global_id`,`site_key`), ADD KEY `actor_id` (`actor_id`);