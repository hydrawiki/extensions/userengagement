<?php
/**
 * Curse Inc.
 * User Engagement
 * User Engagement Special Page
 *
 * @author		Alex Smith
 * @copyright	(c) 2013 Curse Inc.
 * @license		GNU General Public License v2.0 or later
 * @package		User Engagement
 * @link		https://gitlab.com/hydrawiki
 *
 **/

class SpecialUserEngagement extends HydraCore\SpecialPage {
	/**
	 * Output HTML
	 *
	 * @var		string
	 */
	private $content;

	/**
	 * Main Constructor
	 *
	 * @access	public
	 * @return	void
	 */
	public function __construct() {
		parent::__construct('UserEngagement', 'user_engagement');
	}

	/**
	 * Main Executor
	 *
	 * @access	public
	 * @param	string	Sub page passed in the URL.
	 * @return	void	[Outputs to screen]
	 */
	public function execute($subpage) {
		$this->checkPermissions();

		$this->templateUserEngagement = new TemplateUserEngagement;

		$this->output->addModules('ext.userEngagement');

		$this->setHeaders();

		$this->userEngagementSearch();

		$this->output->addHTML($this->content);
	}


	/**
	 * User Engagement Search Page
	 *
	 * @access	public
	 * @return	void	[Outputs to screen]
	 */
	public function userEngagementSearch() {
		$start = $this->wgRequest->getInt('st');
		$itemsPerPage = 50;
		$searchTerm = null;
		$cookieExpire = time() + 900;
		$sortKey = 'user_id';
		$sortDir = 'ASC';

		if ($this->wgRequest->getVal('do') == 'resetSearch') {
			$this->wgRequest->response()->setcookie('userEngagementSearchTerm', '', 1);
		} else {
			$listSearch = $this->wgRequest->getVal('list_search');
			$cookieSearch = $this->wgRequest->getCookie('userEngagementSearchTerm');
			if (($this->wgRequest->getVal('do') == 'search' && !empty($listSearch)) || !empty($cookieSearch)) {
				if (!empty($cookieSearch) && empty($listSearch)) {
					$searchTerm = $this->wgRequest->getCookie('userEngagementSearchTerm');
				} else {
					$searchTerm = $this->wgRequest->getVal('list_search');
				}
				$this->wgRequest->response()->setcookie('userEngagementSearchTerm', $searchTerm, $cookieExpire);
			}
		}

		$filterValues = [
			'x_days_active'	=> 30,
			'total_points'	=> 500,
		];

		$xDaysActive = $this->wgRequest->getVal('x_days_active');
		if ($xDaysActive > 0) {
			$filterValues['x_days_active'] = $xDaysActive;
		}

		$totalPoints = $this->wgRequest->getVal('total_points');
		if ($totalPoints > 0) {
			$filterValues['total_points'] = $totalPoints;
		}

		$api = new ApiMain(new DerivativeRequest(
			$this->wgRequest,
			[
				'action'	=> 'userengagement',
				'do'		=> 'search',
				'maxAge'	=> $filterValues['x_days_active'],
				'minPoints'	=> $filterValues['total_points'],
				'start'		=> $start,
				'items'		=> $itemsPerPage,
				'search'	=> $searchTerm
			]
		));
		$api->execute();
		$userEngagements = $api->getResult()->getResultData();

		$userFilters = $this->wgRequest->getValues('list_search', 'x_days_active', 'total_points');

		$pagination = HydraCore::generatePaginationHtml($this->getFullTitle(), $userEngagements['total_results'], $itemsPerPage, $start, 4, (array) $userFilters);

		$this->output->setPageTitle(wfMessage('userengagement')->escaped());
		$this->content = $this->templateUserEngagement->userEngagementSearch($userEngagements, $pagination, $filterValues, $sortKey, $sortDir, $searchTerm);
	}

	/**
	 * User Engagement Send
	 *
	 * @access	public
	 * @return	mixed	Total engagements successfully sent or false on error.
	 */
	public function userEngagementSend() {
		$api = new ApiMain(new DerivativeRequest(
			$this->wgRequest,
			[
				'action' => 'userengagement',
				'do' => 'send',
				'results' => $this->wgRequest->getArray('results'),
				'title' => $this->wgRequest->getVal('user_engage_title'),
				'message' => $this->wgRequest->getVal('user_engage_message'),
				'username' => $this->wgRequest->getVal('sender_username'),
				'password' => $this->wgRequest->getVal('sender_password'),
				'maxAge' => $this->wgRequest->getVal('x_days_active'),
				'minPoints' => $this->wgRequest->getVal('total_points'),
			]
		));
		try {
			$api->execute();
		} catch (ApiUsageException $e) {
			return ['errors' => [
				'generic' => $e->getMessage()
			]];
		}
		return $api->getResultData();
	}

	/**
	 * Return the group name for this special page.
	 *
	 * @access	protected
	 * @return	string
	 */
	protected function getGroupName() {
		return 'users';
	}
}
