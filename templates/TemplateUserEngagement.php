<?php
/**
 * Curse Inc.
 * User Engagement
 * User Engagement Template
 *
 * @author		Alex Smith
 * @copyright	(c) 2013 Curse Inc.
 * @license		GNU General Public License v2.0 or later
 * @package		User Engagement
 * @link		https://gitlab.com/hydrawiki
 *
**/

class TemplateUserEngagement {
	/**
	 * Output HTML
	 *
	 * @var		string
	 */
	private $HMTL;

	/**
	 * User Engagement Search Page
	 *
	 * @access	public
	 * @param	array	Array of subscription information.
	 * @param	array	Pagination
	 * @param	array	Filter Values
	 * @param	string	[Optional] Data sorting key
	 * @param	string	[Optional] Data sorting direction
	 * @param	string	[Optional] Search Term
	 * @return	string	Built HTML
	 */
	public function userEngagementSearch($results, $pagination, $filterValues, $sortKey = 'user_id', $sortDir = 'ASC', $searchTerm = null) {
		global $wgOut, $wgUser, $wgRequest;

		$userEngagementPage = Title::newFromText('Special:UserEngagement');
		$userEngagementUrl = $userEngagementPage->getFullURL();

		$html = "
			<div class='filter_bar'>
				<form method='get' action='{$userEngagementUrl}'>
					<fieldset>
						<input id='filtervalues' type='hidden' value='".htmlspecialchars(json_encode($filterValues, JSON_UNESCAPED_SLASHES), ENT_QUOTES)."'/>
						<input type='hidden' name='section' value='list'/>
						<input type='hidden' name='do' value='search'/>
						<button type='submit' class='mw-ui-button mw-ui-progressive'>".wfMessage('filter')->escaped()."</button>
						<a href='{$userEngagementUrl}?do=resetSearch' class='mw-ui-button mw-ui-destructive'>".wfMessage('list_reset')->escaped()."</a>
						<input type='text' name='list_search' value='".htmlentities($searchTerm, ENT_QUOTES)."' class='search_field' placeholder='".wfMessage('search_users')->escaped()."'/>

						<label for='x_days_active'' class='label_above'>".wfMessage('active_in_the_last')->escaped()."</label>
						<input id='x_days_active' name='x_days_active' type='text' value='".htmlentities($filterValues['x_days_active'], ENT_QUOTES)."'/>

						<label for='total_points'>".wfMessage('has_at_least')->escaped()."</label>
						<input id='total_points' name='total_points' type='text' value='".htmlentities($filterValues['total_points'], ENT_QUOTES)."'/>
					</fieldset>
				</form>
			</div>
			<div id='userengagement_list'>
				<div>{$pagination}</div>
				<table class='with_filters'>
					<thead>
						<tr class='sortable' data-sort-dir='".($sortDir == 'desc' ? 'desc' : 'asc')."'>
							<th>".wfMessage('ue_th_user')->escaped()."</th>
							<th>".wfMessage('ue_th_points')->escaped()."</th>
							<th>".wfMessage('ue_th_wiki')->escaped()."</th>
							<th>".wfMessage('ue_th_manager')->escaped()."</th>
							<th>".wfMessage('ue_th_engaged')->escaped()."</th>
							<th class='controls'>&nbsp;</th>
						</tr>
					</thead>
					<tbody>
				";
		if (is_array($results['users']) && count($results['users'])) {
			foreach ($results['users'] as $user) {
				$lookup = \CentralIdLookup::factory();
				$userLocal = $lookup->localUserFromCentralId($user['global_id'], \CentralIdLookup::AUDIENCE_RAW);
				$_managerList = '';
				if (!isset($_managerCache[$user['site_key']]) && isset($results['wiki_managers'][$user['site_key']]) && is_array($results['wiki_managers'][$user['site_key']]) && count($results['wiki_managers'][$user['site_key']])) {
					foreach ($results['wiki_managers'][$user['site_key']] as $manager) {
						$_managerList .= "<li>".$manager."</li>";
					}
					$_managerCache[$user['site_key']] = "<ul>".$_managerList."</ul>";
				}
				$html .= "
						<tr>
							<td>".($userLocal !== null ? $userLocal->getName() : $user['global_id'])."</td>
							<td>{$user['score']}</td>
							<td>".(isset($results['wiki_sites'][$user['site_key']]) ? $results['wiki_sites'][$user['site_key']] : "&nbsp;")."</td>
							<td class='manager'>".(isset($_managerCache[$user['site_key']]) ? $_managerCache[$user['site_key']] : "&nbsp;")."</td>
							<td>";
				if (isset($user['engagements'])) {
					$html .= $this->engagementsList($user['engagements']);
				} else {
					$html .= '&nbsp;';
				}
				$html .= "
							</td>
							<td class='controls'>
								<form>
									<fieldset>
										<input type='hidden' name='global_id' value='{$user['global_id']}'/>
										<input type='hidden' name='site_key' value='{$user['site_key']}'/>
										<input type='hidden' name='historical_points' value='{$user['score']}'/>
										<div class='engagement_notes popupWrapper'>
											<div class='popupInner'>
												<label for='name_fill' class='label_above'>".wfMessage('please_enter_message_details')->escaped()."</label>
												<textarea class='engagement_message' name='message'></textarea>

												<div class='button_bar'>
													<div class='buttons_right'>
														<button type='button' class='cancel_engagement mw-ui-button'>".wfMessage('cancel')->escaped()."</button>
														<button type='button' class='save_engagement mw-ui-button'>".wfMessage('save')->escaped()."</button>
													</div>
												</div>
											</div>
										</div>
										<button type='button' class='engaged mw-ui-button'>".wfMessage('engaged')->escaped()."</button>
									</fieldset>
								</form>
							</td>
						</tr>
";
			}
		} else {
			$html .= "
						<tr>
							<td colspan='6'>".wfMessage('no_subscriptions_found')->escaped()."</td>
						</tr>
			";
		}
		$html .= "
					</tbody>
				</table>
				{$pagination}
			</div>";

		return $html;
	}

	/**
	 * Generate a list of user engagements suitable for display or in a table cell.
	 *
	 * @access	private
	 * @param	array	User Engagements
	 * @return	string	Built HTML
	 */
	private function engagementsList($engagements) {
		$html = '';

		foreach ($engagements as $engagement) {
			$actor = User::newFromId($engagement['actor_id']);

			$html .= "<li data-actor-id='{$engagement['ueid']}'>".wfMessage('engagement_list_item', $actor->getId(), $actor->getName(), wfTimestamp(TS_DB, $engagement['sent_timestamp']), $engagement['historical_points'], $engagement['message'])."</li>\n";
		}

		$html = "<ul class='engagement_list'>".$html."</ul>";

		return $html;
	}
}
