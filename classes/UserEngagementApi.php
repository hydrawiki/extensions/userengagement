<?php
/**
 * Curse Inc.
 * User Engagement
 * User Engagement API
 *
 * @author		Noah Manneschmidt
 * @copyright	(c) 2014 Curse Inc.
 * @license		GNU General Public License v2.0 or later
 * @package		User Engagement
 * @link		https://gitlab.com/hydrawiki
 *
**/

class UserEngagementApi extends HydraApiBase {
	/**
	 * Maximum search results to return.
	 *
	 * @var		integer
	 */
	const MAX_SEARCH_RESULTS = 2000;

	/**
	 * Main execution entry point.
	 *
	 * @access	public
	 * @return	mixed
	 */
	public function execute() {
		global $wgUser;

		if (!$wgUser->isAllowed('user_engagement')) {
			return $this->dieWithError('userengagement-invalidaction');
		}

		return parent::execute();
	}

	/**
	 * Return the description for this API end point.
	 *
	 * @access	public
	 * @return	string
	 */
	public function getDescription() {
		return 'Allows users to be queried and messaged based on a variety of criteria.';
	}

	/**
	 * Return the parameter descriptions.
	 *
	 * @access	public
	 * @return	array	Parameter => Description
	 */
	public function getParamDescription() {
		return array_merge(
			parent::getParamDescription(),
			[
				//Search parameters
				'maxAge' => 'The maximum age in number of days an account may be to appear in a search',
				'minPoints' => 'The minimum number of points an account must make in order to appear in a search',

				//Engaged Parameters
				'globalId' => 'Global ID of the user being engaged.',
				'siteKey' => 'Site Key that the user exists on.',
				'message' => 'Message and/or details of an user engagement.',
				'points' => 'Historical points/score at the time of the engagement.'
			]
		);
	}

	/**
	 * Return possible actions for this API.
	 *
	 * @access	public
	 * @return	array
	 */
	public function getActions() {
		return [
			'search' => [
				'tokenRequired' => false,
				'postRequired' => false,
				'params' => [
					'maxAge' => [
						ApiBase::PARAM_TYPE => 'integer',
						ApiBase::PARAM_REQUIRED => false,
					],
					'minPoints' => [
						ApiBase::PARAM_TYPE => 'integer',
						ApiBase::PARAM_REQUIRED => false,
					],
				],
			],
			'engaged' => [
				'tokenRequired' => true,
				'postRequired' => true,
				'params' => [
					'globalId' => [
						ApiBase::PARAM_TYPE => 'integer',
						ApiBase::PARAM_REQUIRED => true
					],
					'siteKey' => [
						ApiBase::PARAM_TYPE => 'string',
						ApiBase::PARAM_REQUIRED => true
					],
					'message' => [
						ApiBase::PARAM_TYPE => 'string',
						ApiBase::PARAM_REQUIRED => true
					]
				]
			]
		];
	}

	/**
	 * Do a search against the database for user information.
	 *
	 * @access	public
	 * @return	void
	 */
	protected function doSearch() {
		$db = wfGetDB(DB_MASTER);

		$xDaysActive = $this->getMain()->getVal('maxAge');
		if ($xDaysActive > 0) {
			$daysAgo = 86400 * intval($xDaysActive);
			$search[] = 'user_global.last_synchronization >= '.intval(time() - $daysAgo);
		}

		$searchTerm = $this->getMain()->getVal('search');
		if (!empty($searchTerm)) {
			$search[] = "user.user_name LIKE '%".$db->strencode($searchTerm)."%'";
		}

		$results = [];
		$siteIds = [];
		if (count($search)) {
			$search[] = 'user_global.global_id > 0';

			$result = $db->select(
				[
					'user_global',
					'user',
					'user_engagements'
				],
				[
					'user_global.*',
					'user.*',
					'user_engagements.ueid',
					'user_engagements.historical_points',
					'user_engagements.sent_timestamp',
					'user_engagements.message',
					'user_engagements.actor_id'
				],
				$search,
				__METHOD__,
				[
					'ORDER BY'	=> 'user_global.last_synchronization ASC',
					'LIMIT'		=> min($this->getMain()->getVal('items'), self::MAX_SEARCH_RESULTS),
					'OFFSET'	=> max($this->getMain()->getVal('start'), 0),
					'SQL_CALC_FOUND_ROWS'
				],
				[
					'user' => [
						'INNER JOIN', 'user.user_id = user_global.user_id'
					],
					'user_engagements' => [
						'LEFT JOIN', 'user_engagements.global_id = user_global.global_id'
					]
				]
			);

			$calcRowsResult = $db->query('SELECT FOUND_ROWS() AS rowcount;');
			$total = $db->fetchRow($calcRowsResult);
			$total = intval($total['rowcount']);

			$wikis = [];
			$managerHashes = [];
			$fakeMainWiki = \DynamicSettings\Wiki::getFakeMainWiki();
			$totalResultsDiff = 0;

			$statProgress = [];
			try {
				$filters = [
					'stat'				=> 'wiki_points',
					'limit'				=> min($this->getMain()->getVal('items'), self::MAX_SEARCH_RESULTS),
					'offset'			=> max($this->getMain()->getVal('start'), 0),
					'sort_direction'	=> 'desc'
				];
				$statProgress = \Cheevos\Cheevos::getStatProgress($filters);
			} catch (\Cheevos\CheevosException $e) {
				wfDebug(__METHOD__.": ".wfMessage('cheevos_api_error', $e->getMessage()));
			}
			$userPoints = [];
			foreach ($statProgress as $progress) {
				$globalId = $progress->getUser_Id();

				if ($globalId < 1) {
					continue;
				}

				$lookupKey = $globalId.'-'.$progress->getSite_Key();
				if (isset($userPoints[$lookupKey])) {
					continue;
				}

				$userPoints[$lookupKey] = $progress;
			}

			while ($row = $result->fetchRow()) {
				$engagement = false;
				if (isset($row['ueid'])) {
					$engagement = [
					    "ueid"				=> $row['ueid'],
					    "historical_points"	=> $row['historical_points'],
					    "sent_timestamp"	=> $row['sent_timestamp'],
					    "message"			=> $row['message'],
					    "actor_id"			=> $row['actor_id']
					];
					$row = array_diff_key($row, $engagement);
				}
				if (!isset($results[$row['global_id'].'-'.$row['site_key']])) {
					$results[$row['global_id'].'-'.$row['site_key']] = $row;
				} else {
					//Users get combined into one entry in so a later SQL_CALC_FOUND_ROWS will be off.  So total results will be SQL_CALC_FOUND_ROWS - $totalResultsDiff.
					$totalResultsDiff++;
				}
				if ($engagement !== false) {
					$results[$row['global_id'].'-'.$row['site_key']]['engagements'][] = $engagement;
				}

				if (!isset($wikis[$row['site_key']])) {
					if ($row['site_key'] === 'master') {
						$wiki = $fakeMainWiki;
					} else {
						$wiki = \DynamicSettings\Wiki::loadFromHash($row['site_key']);
					}
					if ($wiki !== false) {
						$wikis[$row['site_key']] = $wiki->getNameForDisplay();
						$managerHashes[$row['site_key']] = $wiki->getManagers();
						if (is_array($managerHashes[$row['site_key']])) {
							sort($managerHashes[$row['site_key']]);
						}
					}
				}

				if (count($results) >= self::MAX_SEARCH_RESULTS) {
					$this->getResult()->addValue(null, 'maxresultswarning', wfMessage('maxresultsize', self::MAX_SEARCH_RESULTS)->text());
					break;
				}
			}

			$total = $total - $totalResultsDiff;

			asort($wikis);

			$this->getResult()->addValue(null, 'wiki_sites', $wikis);
			$this->getResult()->addValue(null, 'wiki_managers', $managerHashes);
			$this->getResult()->addValue(null, 'total_results', $total);
		}

		$this->getResult()->addValue(null, 'users', $results);
	}

	/**
	 * Update or insert a new user engagement.
	 *
	 * @access	protected
	 * @return	void
	 */
	protected function doEngaged() {
		$success = true;

		$globalId = $this->getMain()->getVal('globalId');
		$siteKey = $this->getMain()->getVal('siteKey');
		$message = $this->getMain()->getVal('message');
		$points = $this->getMain()->getVal('points');

		$ue = new UserEngagement();
		$errors = [];

		$ue->setSentTimestamp(time());
		$ue->setActorId($this->getUser()->getId());
		$ue->setHistoricalPoints($points);

		if (!$ue->setGlobalId($globalId)) {
			$errors['globalId'] = wfMessage('error_ue_api_bad_global_id')->escaped();
		}

		if (!$ue->setSiteKey($siteKey)) {
			$errors['siteKey'] = wfMessage('error_ue_api_bad_site_key')->escaped();
		}

		if (empty($message)) {
			$errors['message'] = wfMessage('error_ue_api_empty_message')->escaped();
		}
		$ue->setMessage($message);

		if (empty($errors)) {
			if ($ue->save() === false) {
				$errors['save'] = wfMessage('error_ue_api_failed_to_save')->escaped();
			}
		}

		if (!empty($errors)) {
			$this->getResult()->addValue(null, 'errors', $errors);
			$success = false;
		}

		$this->getResult()->addValue(null, 'success', $success);
	}
}
