<?php
/**
 * Curse Inc.
 * User Engagement
 * User Engagement Class
 *
 * @author		Alex Smith
 * @copyright	(c) 2013 Curse Inc.
 * @license		GNU General Public License v2.0 or later
 * @package		User Engagement
 * @link		https://gitlab.com/hydrawiki
 *
**/

class UserEngagement {
	/**
	 * Engagement Data for this instance.
	 *
	 * @var		array
	 */
	protected $engagement = [];

	/**
	 * Errors that occurred manipulating data that prevents saving.
	 *
	 * @var		array
	 */
	protected $errors = [];

	/**
	 * Loaded from the database?
	 *
	 * @var		boolean
	 */
	protected $loadedFromDatabase = false;

	/**
	 * Load a new instance from a database row.
	 *
	 * @access	public
	 * @return	mixed	Instantiated UserEngagement object or false on failure.
	 */
	static public function loadFromRow($row) {
		$userEngagement = new UserEngagement();

		if ($userEngagement->load($row)) {
			return $userEngagement;
		}

		return false;
	}

	/**
	 * Load an engagement from a database row.
	 *
	 * @access	public
	 * @param	array	Database row to load.
	 * @return	boolean	Success
	 */
	private function load($engagement) {
		if ($engagement['ueid'] > 0) {
			$this->engagement = $engagement;
			$this->loadedFromDatabase = true;
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Save an Engagement to the Database
	 *
	 * @access	public
	 * @return	true	Boolean success
	 */
	public function save() {
		if (count($this->errors)) {
			return false;
		}

		$db = wfGetDB(DB_MASTER);

		if (is_array($this->engagement) && array_key_exists('ueid', $this->engagement) && $this->loadedFromDatabase) {
			$db->update(
				'user_engagements',
				$this->engagement,
				['ueid' => intval($this->engagement['ueid'])],
				__METHOD__
			);
			$db->commit();
		} else {
			$db->insert(
				'user_engagements',
				$this->engagement,
				__METHOD__
			);
			$db->commit();
		}

		return true;
	}

	/**
	 * Get errors set by the set functions.
	 *
	 * @access	public
	 * @return	mixed	Array of errors or false for no errors.
	 */
	public function getErrors() {
		return (count($this->errors) ? $this->errors : false);
	}

	/**
	 * Get the engagement ID saved for this engagement.
	 *
	 * @access	public
	 * @return	mixed	Engagement ID from the database or false for an unsaved engagement.
	 */
	public function getId() {
		return ($this->engagement['ueid'] ? $this->engagement['ueid'] : false);
	}

	/**
	 * Get the Curse ID for this engagement.
	 *
	 * @access	public
	 * @return	mixed	Curse ID or false on failure.
	 */
	public function getGlobalId() {
		$globalId = intval($this->engagement['global_id']);
		return ($globalId ? $globalId : false);
	}

	/**
	 * Set the Curse ID
	 *
	 * @access	public
	 * @param	integer	Curse ID
	 * @return	boolean	True for a valid Global ID or false for an invalid Global ID.
	 */
	public function setGlobalId($globalId) {
		if ($globalId > 0) {
			$this->engagement['global_id'] = intval($globalId);
			$success = true;
		} else {
			$success = false;
		}

		if (!$success) {
			$this->errors['global_id'] = 'INVALID_GLOBAL_ID';
		} else {
			unset($this->errors['global_id']);
		}

		return $success;
	}

	/**
	 * Get the DynamicSettings site key for this engagement.
	 *
	 * @access	public
	 * @return	string	Site Key - Typically a 32 character MD5 hash.
	 */
	public function getSiteKey() {
		return $this->engagement['site_key'];
	}

	/**
	 * Set the DynamicSettings site key for this engagement.
	 *
	 * @access	public
	 * @return	string	Site Key - Typically a 32 character MD5 hash.
	 * @return	boolean	Successfully set, false if the site does not exist.
	 */
	public function setSiteKey($siteKey) {
		if ($siteKey !== 'master') {
			$wiki = \DynamicSettings\Wiki::loadFromHash($siteKey);

			if ($wiki === false) {
				return false;
			}
		}

		$this->engagement['site_key'] = $siteKey;

		return true;
	}

	/**
	 * Get the historical wiki points total.
	 *
	 * @access	public
	 * @return	integer	Total points.
	 */
	public function getHistoricalPoints() {
		return intval($this->engagement['historical_points']);
	}

	/**
	 * Set the historical wiki points total.
	 *
	 * @access	public
	 * @param	integer	Number of points.
	 * @return	void
	 */
	public function setHistoricalPoints($points) {
		$this->engagement['historical_points'] = intval($points);
	}

	/**
	 * Get the timestamp of when the engagement was sent.
	 *
	 * @access	public
	 * @return	integer	Unix Timestamp
	 */
	public function getSentTimestamp() {
		return intval($this->engagement['sent_timestamp']);
	}

	/**
	 * Set the timestamp of when the engagement was sent.
	 *
	 * @access	public
	 * @param	integer	Unix Timestamp
	 * @return	void
	 */
	public function setSentTimestamp($timestamp) {
		$this->engagement['sent_timestamp'] = intval($timestamp);
	}

	/**
	 * Get the message
	 *
	 * @access	public
	 * @return	string	Message
	 */
	public function getMessage() {
		return $this->engagement['message'];
	}

	/**
	 * Set the message.
	 *
	 * @access	public
	 * @param	string	Message
	 * @return	void
	 */
	public function setMessage($message) {
		$this->engagement['message'] = trim($message);
	}

	/**
	 * Get the actor ID.
	 *
	 * @access	public
	 * @return	integer	Actor ID - The user who performed the action.
	 */
	public function getActorId() {
		return intval($this->engagement['actor_id']);
	}

	/**
	 * Set the actor ID.
	 *
	 * @access	public
	 * @param	integer	Actor ID - The user who performed the action.
	 * @return	void
	 */
	public function setActorId($actorId) {
		$this->engagement['actor_id'] = intval($actorId);
	}
}